﻿using UnityEngine;
using LitJson;
using UnityEngine.UI;

public class ReadData : MonoBehaviour {

    public GameObject level1;
    public GameObject level2;
    
    public Text objective;
    public Text firstTokenName;
    public Text firstTokenStatus;
    public Text secondTokenName;
    public Text secondTokenStatus;

    private JsonData _parsedData;    
    class _parsedClass{
        public string objective;
        public string[] _firstTokenDetails = new string[2];
        public string[] _secondTokenDetails = new string[2];
    }
    private _parsedClass _currentObj;

    void Start () {

        _currentObj = new _parsedClass();

        TextAsset t = Resources.Load<TextAsset>("level");
        convertJson(t.text);

        DisplayLevel1();
	}

    /// <summary>
    /// convert the data into array
    /// </summary>
    /// <param name="data">data</param>
    void convertJson(string data)
    {
        _parsedData = JsonMapper.ToObject(data);
    }

    /// <summary>
    /// process the data of level 1
    /// </summary>
    public void DisplayLevel1()
    {
        _currentObj.objective = (string)_parsedData["GameData"][0]["Objective"];

        _currentObj._firstTokenDetails[0] = (string)_parsedData["GameData"][0]["Token_Details"][0]["Name"];
        _currentObj._firstTokenDetails[1] = (string)_parsedData["GameData"][0]["Token_Details"][0]["Status"];
        _currentObj._secondTokenDetails[0] = (string)_parsedData["GameData"][0]["Token_Details"][1]["Name"];
        _currentObj._secondTokenDetails[1] = (string)_parsedData["GameData"][0]["Token_Details"][1]["Status"];

        DisplayData();
        level2.SetActive(false);
        level1.SetActive(true);
    }

    /// <summary>
    /// process the data of level 2
    /// </summary>
    public void DisplayLevel2()
    {
        _currentObj.objective = (string)_parsedData["GameData"][1]["Objective"];

        _currentObj._firstTokenDetails[0] = (string)_parsedData["GameData"][1]["Token_Details"][0]["Name"];
        _currentObj._firstTokenDetails[1] = (string)_parsedData["GameData"][1]["Token_Details"][0]["Status"];
        _currentObj._secondTokenDetails[0] = (string)_parsedData["GameData"][1]["Token_Details"][1]["Name"];
        _currentObj._secondTokenDetails[1] = (string)_parsedData["GameData"][1]["Token_Details"][1]["Status"];

        DisplayData();
        level1.SetActive(false);
        level2.SetActive(true);
    }

    /// <summary>
    /// print the current data on screen
    /// </summary>
    void DisplayData()
    {
        objective.text = _currentObj.objective;
        firstTokenName.text = _currentObj._firstTokenDetails[0];
        firstTokenStatus.text = _currentObj._firstTokenDetails[1];
        secondTokenName.text = _currentObj._secondTokenDetails[0];
        secondTokenStatus.text = _currentObj._secondTokenDetails[1];
    }
}
