﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ScreenController : MonoBehaviour {

    public GameObject menu;
    public GameObject level;
    public GameObject gameover;

    public Text sessionText;
    public Text playText;
    public Text totalSessionText;
    public Text levelText;

    private float sessionTime;
    private float playTime;
    private float previousSessionTime;
    private float levelTime;
    private float minimizedTime = 0f;
    private float TimeBeforeMinimize = 0f;
    private bool _isPaused = false;

    /// <summary>
    /// time in seconds, when the game started
    /// </summary>
    private float gameStartTime;    


    void Start () {
        
        //considering only minutes and seconds
        gameStartTime = (DateTime.Now.Minute * 60) + DateTime.Now.Second;
        if (File.Exists(Application.persistentDataPath + "/total.txt"))
            previousSessionTime = float.Parse(File.ReadAllText(Application.persistentDataPath + "/total.txt"));
    }
	

	void Update () {

        UpdateSessionTime();
        UpdatetotalSessionTime();

        if (level.activeInHierarchy)
        {
            UpdatelevelTime();
            UpdateplayTime();
        }
	}

    /// <summary>
    /// current session Time
    /// </summary>
    void UpdateSessionTime()
    {
        sessionTime = ((DateTime.Now.Minute * 60) + DateTime.Now.Second) - gameStartTime + minimizedTime;
        sessionText.text = ((int)(sessionTime)).ToString();
    }

    /// <summary>
    /// time user spent on level including minimized time
    /// </summary>
    void UpdateplayTime()
    {
        playTime += Time.deltaTime;
        playText.text = ((int)(playTime + minimizedTime)).ToString();
    }

    /// <summary>
    /// total session time - previous session time + current session time
    /// </summary>
    void UpdatetotalSessionTime()
    {
        totalSessionText.text = ((int)(sessionTime + previousSessionTime)).ToString();
    }
    
    /// <summary>
    /// time user spent on level excluding minimized time
    /// </summary>
    void UpdatelevelTime()
    {
        levelTime += Time.deltaTime;
        levelText.text = ((int)(levelTime - minimizedTime)).ToString();
    }

    /// <summary>
    /// start the level,
    /// called from a button click
    /// </summary>
    public void StartGame()
    {
        menu.SetActive(false);
        level.SetActive(true);
    }

    /// <summary>
    /// game is in background
    /// </summary>
    void PauseGame()
    {
        TimeBeforeMinimize = (DateTime.Now.Minute*60) + DateTime.Now.Second;
        _isPaused = true;
        File.WriteAllText(Application.persistentDataPath + "/total.txt", (sessionTime+previousSessionTime).ToString());
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
            CalculatePauseTime();
        else
            PauseGame();
    }

    /// <summary>
    /// game is again in foreground
    /// </summary>
    void CalculatePauseTime()
    {
        if (!_isPaused)
            return;
        
        minimizedTime = ((DateTime.Now.Minute * 60) + DateTime.Now.Second) - TimeBeforeMinimize;
        UpdateSessionTime();
        _isPaused = false;
    }
}
